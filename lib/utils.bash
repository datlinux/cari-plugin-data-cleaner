#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/datacleaner/DataCleaner"
TOOL_NAME="data-cleaner"
TOOL_TEST="data-cleaner"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | 
    sed 's/^v//' | sed 's/^refs\/tags\/DataCleaner-//'
}

list_all_versions() {
  list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  url="https://github.com/datacleaner/DataCleaner/releases/download/DataCleaner-$version/DataCleaner-$version.zip"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"

    curl -L -o $install_path/jre.tar.gz "https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.2%2B13/OpenJDK21U-jre_x64_linux_hotspot_21.0.2_13.tar.gz"
    tar -C "$install_path/" -xf "$install_path/jre.tar.gz"
    rm "$install_path/jre.tar.gz"
    grep -v "exec java" "$install_path/DataCleaner/datacleaner.sh" > "$install_path/DataCleaner/datacleaner.sh.tmp" && mv "$install_path/DataCleaner/datacleaner.sh.tmp" "$install_path/DataCleaner/datacleaner.sh"
    jpath="$(compgen -G $install_path/*/bin/java)"
    echo "$jpath \$DATACLEANER_JAVA_OPTS -jar \$DATACLEANER_LIB_HOME/DataCleaner.jar \"\$@\"" >> "$install_path/DataCleaner/datacleaner.sh"

    chmod a+x "$install_path/DataCleaner/datacleaner.sh"
    touch "$install_path/bin/data-cleaner"
    echo "#!/bin/bash" >> "$install_path/bin/data-cleaner"
    echo "bash $install_path/DataCleaner/datacleaner.sh" >> "$install_path/bin/data-cleaner"
    chmod a+x "$install_path/bin/data-cleaner"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
